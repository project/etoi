/**
 * @file
 * Contains global-etoi.js.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.etoiBehavior = {
    attach: function (context, settings) {
      var full_url = window.location.protocol + '//' + window.location.host;
      if ($('.etoi')[0]) {
        $('.etoi').once().each(function (index, value) {

          // Select hash from email generated hash.
          var hash = $('.etoi:eq(' + index + ')').text();

          // Get the base url from .module file.
          // Get the styles from the element we are going to replace.
          font_size = getStyle(value, 'font-size');
          font_family = getStyle(value, 'font-family');
          font_color = getStyle(value, 'color');

          // Font Weight values, 400 Normal 700 Bold.
          font_weight = getStyle(value, 'font-weight');
          font_style = getStyle(value, 'font-style');

          // Create css data array.
          css_data = [];

          // Remove px from computed font-size and set the nearest integer value.
          css_data[0] = parseInt(font_size) - 1;
          css_data[1] = font_color;
          css_data[2] = font_family;
          css_data[3] = font_weight;
          css_data[4] = font_style;
          $.ajax({
            type: 'GET',
            url: full_url + "/etoi/gir?data=" + hash,
            dataType: "json",
            data: { css_data: JSON.stringify(css_data) },
          }).then(function (data) {

            // Get the final image url.
            var src_url = data['data'];
            var img_url = '<img src="' + src_url + '">';
            $('.etoi:eq(' + index + ')').html(img_url);
          });
        });
      }

      function getStyle(el,styleProp) {
        var camelize = function (str) {
          return str.replace(/\-(\w)/g, function (str, letter) {
            return letter.toUpperCase();
          });
        };

        if (el.currentStyle) {
          return el.currentStyle[camelize(styleProp)];
        }
        else if (document.defaultView && document.defaultView.getComputedStyle) {
          return document
            .defaultView
            .getComputedStyle(el,null)
            .getPropertyValue(styleProp);
        }
        else {
          return el.style[camelize(styleProp)];
        }
      }

      $('a[href^="mailto:"]').each(function (index) {
        var anchor = $(this);
        anchor.attr('rel', 'nofollow');
        anchor.attr('target', '_blank');
        var hash = anchor.attr('href').replace('mailto:', full_url + '/form/mailto?hid=');
        $(this).attr('href', hash);
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
