<?php

namespace Drupal\etoi;

use Drupal\Core\Messenger\MessengerInterface;

/**
 * Class description.
 *
 * @see \Drupal\etoi\Controller\DeleteFiles
 */
class DeleteFiles {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * MyModuleService constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function deleteFiles(&$context) {
    $message = 'Deleting Files...';
    $delfiles = [];
    $files = file_scan_directory('public://etoi', '(.*)');

    // Delete files on status 0 and  at the same
    // time from database file_managed.
    $fonturi = 'public://my_fonts';
    $database = \Drupal::database();
    $query = $database->select('file_managed', 'fm')
      ->fields('fm', ['fid', 'status', 'uri'])
      ->condition('uri', "%" . $database->escapeLike($fonturi) . "%", 'LIKE')
      ->execute()
      ->fetchAll();

    foreach ($query as $key => $row) {
      unset($query[$key]);
      foreach ($query as $row2) {
        if (($row->uri == $row2->uri)&&($row->status != $row2->status)) {
          if ($row->status == 0) {
            $delfiles[] = $row->fid;
          }
          else {
            $delfiles[] = $row2->fid;
          }

        }
      }
    }

    if (!empty($delfiles)) {
      foreach ($delfiles as $delfile) {
        $database->delete('file_managed')
          ->condition('fid', $delfile)
          ->execute();
      }
    }

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['current_id'] = 0;
      $context['sandbox']['max'] = count($files);
    }

    foreach ($files as $file) {
      $context['results'][] = check_markup($file->uri);
      $context['sandbox']['progress']++;
      file_unmanaged_delete($file->uri);
      $context['message'] = check_markup($file->uri);
      if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
      }

    }

  }

  /**
   * {@inheritdoc}
   */
  public static function deleteFilesFinishedCallback($success, $results, $operations) {
    if ($success) {
      $message = t('All files Deleted.');
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
    /*
     * @todo use messenger interface instead of drupal set message.
     */
    // $this->messenger()->addMessage($message);
  }

}
