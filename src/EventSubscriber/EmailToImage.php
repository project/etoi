<?php

namespace Drupal\etoi\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Replace email for image when drupal get the request response.
 *
 * @see \Symfony\Component\EventDispatcher\EventSubscriberInterface
 */
class EmailToImage implements EventSubscriberInterface {
  /**
   * The hask key token.
   *
   * @var string
   */
  private $token;

  /**
   * The hask key token.
   *
   * @var string
   */
  private $ivtoken;

  /**
   * The enable checkbox.
   *
   * @var bool
   */
  private $isEnable;

  /**
   * The content where the class is going to search for emails.
   *
   * @var string
   */
  private $content;

  /**
   * Constructs a Email To Image Object bringing back form variables.
   */
  public function __construct() {
    // Here will load the hash key from admin field.
    $config = \Drupal::config('etoi.adminsettings');
    $this->token = $config->get('hash_key');
    $this->ivtoken = $config->get('iv_key');
    $this->isEnable = $config->get('enable');
  }

  /**
   * Response the HTML.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   Event.
   */
  public function response(FilterResponseEvent $event) {

    // If the replacement is Enabled.
    if ($this->isEnable == '1') {
      /** @var \Drupal\Core\Routing\AdminContext $admin_context */
      $admin_context = \Drupal::service('router.admin_context');

      // If i am not editing a content, do the replacement.
      if (!$admin_context->isAdminRoute()) {
        $response = $event->getResponse();
        $this->content = $response->getContent();
        $email_list = $this->extractEmailsFrom($this->content);

        // Replace email with new data.
        foreach ($email_list as $email) {
          // If Mailto Link exist.
          if (strpos($email, 'mailto:') !== FALSE) {
            preg_match_all('/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i', $email, $womailto);
            $final_string = '<a href="mailto:' . $this->generateCryptedEmail($womailto[0][0]);

            // If the open quotation is single, replace de
            // final string with the single one.
            if (strpos($email, "'") !== FALSE) {
              $final_string = '<a href=\'mailto:' . $this->generateCryptedEmail($womailto[0][0]);
            }
          }
          else {
            $enc_value = $this->generateCryptedEmail($email);
            $final_string = "<span class=\"etoi\" style=\"display: inline-block;\">" . $enc_value . "</span>";
          }
          $this->content = str_replace($email, $final_string, $this->content);
        }
        $response->setContent($this->content);
      }
    }
  }

  /**
   * Get Emails from content.
   *
   * @param string $content
   *   Content.
   */
  public function extractEmailsFrom($content) {
    preg_match_all('/(\<a([^>]+)href\=\"?\'?mailto:)?[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,4})(?:\.[a-z]{2})?/i', $content, $matches);

    return $matches[0];
  }

  /**
   * Generate encrypted email.
   *
   * @param string $email
   *   Email value.
   */
  public function generateCryptedEmail($email) {
    $key = hash('sha256', $this->token);
    $iv = substr(hash('sha256', $this->ivtoken), 0, 16);
    $encrypted_email = openssl_encrypt($email, 'aes256', $key, 0, $iv);
    $encrypted_email = base64_encode($encrypted_email);

    return $encrypted_email;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Get the response event and do "response" method before rendering.
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['response', -10000];

    return $events;
  }

}
