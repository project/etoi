<?php

namespace Drupal\etoi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Class description.
 *
 * @see Drupal\etoi\Form\EmailConfigForm
 */
class EmailConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'etoi.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'etoi_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('etoi.adminsettings');
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable'),
      '#description' => $this->t('Enable Email to Image replacement'),
      '#default_value' => $config->get('enable'),
    ];
    $form['hash_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key'),
      '#description' => $this->t('Key word to encrypt and decrypt text'),
      '#default_value' => $config->get('hash_key'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];
    $form['iv_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IV Key'),
      '#description' => $this->t('Initial Vector Key'),
      '#default_value' => $config->get('iv_key'),
      '#size' => 60,
      '#maxlength' => 64,
      '#required' => TRUE,
    ];
    $validators = [
      'file_validate_extensions' => ['ttf'],
    ];
    $form['my_fonts'] = [
      '#type' => 'managed_file',
      '#name' => 'my_fonts',
      '#title' => t('Fonts upload'),
      '#multiple' => TRUE,
      '#description' => t('TTF format only'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://my_fonts/',
      '#default_value' => $config->get('my_fonts'),
    ];
    $form['actions']['clear_all'] = [
      '#type' => 'submit',
      '#value' => t('Clean Images Folder'),
      '#submit' => ['::clearAllSubmit'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save font files on public folder.
    $fontfiles = $form_state->getValue('my_fonts');
    if (!empty($fontfiles)) {
      foreach ($fontfiles as $fontfile) {
        $file = File::load($fontfile);
        $uri = $file->getFileUri();
        $file->setPermanent();

        // Save the file in the database.
        $file->save();
      }
    }

    // Save config form variables.
    $this->config('etoi.adminsettings')
      ->set('hash_key', $form_state->getValue('hash_key'))
      ->set('iv_key', $form_state->getValue('iv_key'))
      ->set('enable', $form_state->getValue('enable'))
      ->set('my_fonts', $form_state->getValue('my_fonts'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function clearAllSubmit(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Deleting Files...'),
      'operations' => [
        [
          '\Drupal\etoi\DeleteFiles::deleteFiles',
          [],
        ],
      ],
      'finished' => '\Drupal\etoi\DeleteFiles::deleteFilesFinishedCallback',
    ];
    batch_set($batch);
  }

}
