<?php

namespace Drupal\etoi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class description.
 *
 * @see Drupal\etoi\Form\MailToForm
 */
class MailToForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mail_to_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['mail_to'] = [
      '#type' => 'hidden',
      '#title' => $this->t('Mail To:'),
      '#required' => TRUE,
    ];
    $form['mail_from'] = [
      '#type' => 'email',
      '#title' => $this->t('Mail From:'),
      '#placeholder' => $this->t('Enter your email address here'),
      '#required' => TRUE,
    ];
    $form['subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Subject'),
      '#placeholder' => $this->t('Enter the email Subject here'),
      '#required' => TRUE,
    ];
    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Email Message'),
      '#placeholder' => $this->t('Enter the email message here'),
      '#required' => TRUE,
    ];
    $form['captcha'] = [
      '#type' => 'captcha',
      '#captcha_type' => 'image_captcha/Image',
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#button_type' => 'primary',
    ];
    $form['#theme'] = 'etoi_form';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Instance the controller for email decryption.
    $etoiclass = 'Drupal\etoi\Controller\EmailToImageController';
    $etoiobject = new $etoiclass();

    // Get encrypted email from form, and then decrypted and send mail.
    $email = $form_state->getValue('mail_to');
    $to = $etoiobject->generateDecryptedEmail($email);
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'etoi';
    $key = 'mailto';
    $params['mailfrom'] = $form_state->getValue('mail_from');
    $params['message'] = $form_state->getValue('message');
    $params['subject'] = $form_state->getValue('subject');
    $langcode = \Drupal::currentUser()->getPreferredLangcode();
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    drupal_set_message('Thank you for your message, we will contact you soon');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {}

}
