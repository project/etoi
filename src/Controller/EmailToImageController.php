<?php

namespace Drupal\etoi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\file\Entity\File;

/**
 * Controller routines for etoi routes.
 */
class EmailToImageController extends ControllerBase {

  /**
   * The hask key token.
   *
   * @var string
   */
  private $token;

  /**
   * The hask key token.
   *
   * @var string
   */
  private $ivtoken;

  /**
   * The full Path.
   *
   * @var string
   */
  private $fullPath;

  /**
   * The font files array.
   *
   * @var array
   */
  private $files;

  /**
   * The md5 full hash.
   *
   * @var string
   */
  private $newhash;

  /**
   * Constructs a Email To Image Object bringing back form variables.
   */
  public function __construct() {
    // Here will load the hash key from admin field.
    $config = \Drupal::config('etoi.adminsettings');
    $this->token = $config->get('hash_key');
    $this->ivtoken = $config->get('iv_key');
    $this->files = $config->get('my_fonts');
  }

  /**
   * Callback for getImageRoute API method.
   *
   * @todo Create default image and route in case not found error.
   */
  public function getImageRoute(Request $request) {
    $image_data = $request->query->get('data');
    $css_data = json_decode($request->query->get('css_data'));
    $font_family = explode(",", $css_data[2]);
    $font_weight = 'Normal';

    if ($css_data[3] == 700) {
      $font_weight = 'Bold';
    }

    $font_style = $css_data[4];

    // Get Decripted email address.
    $dec_email = $this->generateDecryptedEmail($image_data);

    // Get module and fonts path to read the font.
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('etoi')->getPath();
    $module_fpath = $_SERVER['DOCUMENT_ROOT'] . "/" . $module_path;
    $fonts_path = $_SERVER['DOCUMENT_ROOT'] . '/sites/default/files/my_fonts/';

    // Set default font.
    $font = $module_fpath . '/fonts/OpenSans-Regular.ttf';

    // Search and get custom fonts.
    $custom_font = $this->getFont($font_family, $font_weight, $font_style, $fonts_path);
    if (!empty($custom_font)) {
      $font = $fonts_path . $custom_font;
    }

    // Create Image.
    if ($this->createImageFromEmail($dec_email, $image_data, $css_data, $font)) {
      $image_path = \Drupal::request()->getSchemeAndHttpHost() . '/sites/default/files/etoi/';
      $path = $image_path . $this->newhash . '.png';
      $response['data'] = $path;
    }
    else {
      // Create dafault image in case error or not found.
      $response['data'] = \Drupal::request()->getSchemeAndHttpHost();
    }
    return new JsonResponse($response);
  }

  /**
   * Get the original encrypted email.
   *
   * @param string $string
   *   Crypted string.
   */
  public function generateDecryptedEmail($string) {
    $key = hash('sha256', $this->token);
    $iv = substr(hash('sha256', $this->ivtoken), 0, 16);
    $decrypted_email = openssl_decrypt(base64_decode($string), 'aes256', $key, 0, $iv);
    return $decrypted_email;
  }

  /**
   * Callback to create image.
   */
  public function getColor($string) {
    if (preg_match('!\(([^\)]+)\)!', $string, $match)) {
      return $match[1];
    }
  }

  /**
   * Callback to search and get custom fonts.
   */
  public function getFont($font_family, $font_weight, $font_style, $fonts_path) {
    // Load config fonts files.
    if (!empty($this->files)) {
      foreach ($this->files as $fid) {
        $filel = File::load($fid);
        $furi = $filel->getFileUri();
        $fname = str_replace("public://my_fonts/", "", $furi);
        $fontf_search = '/(' . $font_family[0] . ')/i';
        if (preg_match($fontf_search, $fname)) {
          $allfonts[] = $fname;
          if (preg_match('/(Bold)/i', $fname)) {
            $bfont[] = $fname;
          }
          else {
            $regfont[] = $fname;
          }
        }
      }

      // Process Bold font.
      if ($font_weight == 'Bold') {
        foreach ($bfont as $wfont) {
          // Search inside Bold if there is bold italic.
          if ((preg_match('/(italic)/i', $font_style)) && (preg_match('/(italic)/i', $wfont))) {
            return $wfont;
          }
          elseif ((preg_match('/(italic)/i', $font_style)) && (!preg_match('/(italic)/i', $wfont))) {
            if (count($bfont) < 2) {
              return $wfont;
            }
          }
          elseif ((!preg_match('/(italic)/i', $font_style)) && (!preg_match('/(italic)/i', $wfont))) {
            return $wfont;
          }
          elseif ((!preg_match('/(italic)/i', $font_style)) && (preg_match('/(italic)/i', $wfont))) {
            if (count($bfont) < 2) {
              return $wfont;
            }
          }
        }
      }

      // Process Italic Style.
      if (preg_match('/\italic\b/i', $font_style)) {
        foreach ($regfont as $ifont) {
          preg_match('/\italic\b/i', $ifont, $match);
          if (!empty($match)) {
            return $ifont;
          }
        }
      }

      // Search for regular fonts.
      $font_search = '/^(?!.*Bold|.*Italic).*$/im';
      foreach ($allfonts as $afont) {
        preg_match($font_search, $afont, $match);
        if (!empty($match)) {
          return $afont;
        }
      }

      // If there is not regular assign any font-family font.
      foreach ($allfonts as $afont) {
        preg_match('/(' . $font_family[0] . ')/i', $afont, $match);
        if (!empty($match)) {
          return $afont;
        }
      }
      // If there is no matches return default font.
      return FALSE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Callback to create image.
   */
  public function createImageFromEmail($email, $hash, $css_data, $font) {
    // Measure the email text width and height in pixels.
    $bbox = imagettfbbox($css_data[0], 0, $font, $email);
    $xpx = abs($bbox[2] - $bbox[0]);
    $ypx = abs($bbox[7] - $bbox[1]);
    $font_color = explode(',', $this->getColor($css_data[1]));

    // Create Image with final measures pending on font.
    $image = imagecreatetruecolor($xpx, $ypx);
    imagealphablending($image, TRUE);
    imagesavealpha($image, TRUE);
    $bgcolor = imagecolorallocatealpha($image, 0, 0, 0, 127);
    imagefill($image, 0, 0, $bgcolor);
    $textcolor = imagecolorclosest($image, $font_color[0], $font_color[1], $font_color[2]);
    imagettftext($image, $css_data[0], 0, 0, $css_data[0], $textcolor, $font, $email);

    if ($image !== TRUE) {
      $fsize = $css_data[0];
      $fcolor = $css_data[1];
      $ffamily = explode(",", $css_data[2]);
      $fweight = $css_data[3];
      $fstyle = $css_data[4];
      $final_string = $hash . $ffamily[0] . $fsize . $fcolor . $fweight . $fstyle;
      $this->newhash = md5($final_string);

      // Create Path and full path.
      $path = $_SERVER['DOCUMENT_ROOT'] . '/sites/default/files/etoi';
      $this->fullPath = $path . '/' . $this->newhash . '.png';
      // Validate if directory and image file exist, if dont, create them.
      if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        if (!file_exists($this->fullPath)) {
          $resp = imagepng($image, $this->fullPath);
          imagedestroy($image);
        }
      }
      return TRUE;
    }
    else {
      echo 'An error occurred.';
      return FALSE;
    }
  }

}
